#!/bin/sh
wget https://repo.continuum.io/miniconda/Miniconda2-latest-Linux-x86_64.sh -O Miniconda2-latest-Linux-x86_64.sh
if [ $? -ne 0 ]; then
  echo "Error: Failed to download Miniconda bootstrap script!"
  exit 1
fi

MINICONDA_HOME=./lib/miniconda2
MULTIQC_ENV=multiqc_env

sh Miniconda2-latest-Linux-x86_64.sh -b -p $MINICONDA_HOME
if [ $? -ne 0 ]; then
  echo "Error: Failed to bootstrap Miniconda!"
  exit 1
fi

## Set up environment if not already there
if [ -d $MINICONDA_HOME/envs/$MULTIQC_ENV ]; then
  timestamp=`date "+%Y%m%d-%H%M%S"`
  echo "Existing MultiQC environment, moving to '$MINICONDA_HOME/envs/${MULTIQC_ENV}.$timestamp"
  mv $MINICONDA_HOME/envs/$MULTIQC_ENV $MINICONDA_HOME/envs/${MULTIQC_ENV}.$timestamp
fi

## silent (batch) mode
$MINICONDA_HOME/bin/conda create --name $MULTIQC_ENV -y python=2.7
if [ $? -ne 0 ]; then
  echo "Error: Failed to create Conda environment for MultiQC!"
  exit 1
fi
source $MINICONDA_HOME/bin/activate $MULTIQC_ENV
## Install MultiQC from bioconda channel (silent)
conda install -c bioconda -y  multiqc
if [ $? -ne 0 ]; then
  echo "Error: Failed to install MultiQC into Conda environment!"
  exit 1
fi
vers=`basename \`pwd\``
echo; echo "If there were no error messages, setup for MultiQC $vers completed successfully."
