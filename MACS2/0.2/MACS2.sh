#/bin/sh
##===========================================================================
## Module:   MACS2
## Author:   Othmar Korn
##
## Wrapper for MACS2 ChIP-seq peak caller
##===========================================================================

# Include S4M functions
for i in `ls $S4M_SCRIPT_HOME/inc/*.sh`; do . $i; done

## ============================================================================

. ./inc/targets_file_utils.sh
. ./inc/MACS2_funcs.sh


## Run MACS2 over a given ChIP-seq library (with or without matched control)
##
## Args:
##   target: Row from targets file
##   attr_pos_map: Targets "<column>:<pos>" multi-line string (one line per attribute)
##
macs2_run_target () {
  m2r_targetline="$1"
  m2r_attr_pos_map="$2"

  m2r_sampleid=`fileutil_extract_token_by_name "$m2r_targetline" "$m2r_attr_pos_map" SampleID`
  m2r_readfile1=`fileutil_extract_token_by_name "$m2r_targetline" "$m2r_attr_pos_map" ReadFile1`
  m2r_controlid=`fileutil_extract_token_by_name "$m2r_targetline" "$m2r_attr_pos_map" ControlID`

  m2r_ext_whitelist="bam bed"
  ## If multiple hits, take the first one
  m2r_treatfile=`macs2_find_files_by_basename "$m2r_sampleid" "$MACS2_inputdir" "$m2r_ext_whitelist" | head -1`
  if [ $? -ne 0 ]; then
    s4m_error "Failed to find input treatment file for sample '$m2r_sampleid' (allowed extensions: $m2r_ext_whitelist)!"
  else
    s4m_log "Using treat file:  $m2r_treatfile"
  fi
  m2r_controlfile=`macs2_find_files_by_basename "$m2r_controlid" "$MACS2_inputdir" "bam bed"`


  ## Base args regardless of peak type
  ## --verbose 3 (or higher) will give details peak stats per chromosome etc.
  m2r_args="callpeak  --treatment $m2r_treatfile --outdir $MACS2_outputdir -n $m2r_sampleid --call-summits -B --verbose 3"

  if [ ! -z "$m2r_controlfile" ]; then
    if [ ! -f "$m2r_controlfile" ]; then
      s4m_error "Control library '$m2r_controlfile' not found for sample '$m2r_sampleid'!"
      return 1
    else
      s4m_log "Using control file:  $m2r_controlfile"
    fi
    m2r_args="$m2r_args -c $m2r_controlfile"
  fi

  ## Default "regular" peaks
  if [ -z "$MACS2_peaktype" ]; then
    MACS2_peaktype="regular"
  fi

  ## Set parameters for broad or regular peaks
  ## See MACS2 docs for recommendations.
  if [ "$MACS2_peaktype" = "broad" ]; then
    if [ -z "$MACS2_qvalue" ]; then
      ## MACS2 docs recommend 0.05 qvalue for broad peaks
      m2r_broadqval=0.05
    else
      m2r_broadqval="$MACS2_qvalue"
    fi
    m2r_args="$m2r_args --broad --broad-cutoff 0.1 --qvalue $m2r_broadqval"
    s4m_log "Optimising for 'broad' peaks"

  ## Assume "regular" peaks
  else
    if [ ! -z "$MACS2_qvalue" ]; then
      m2r_args="$m2r_args --qvalue $MACS2_qvalue"
    ## Our default q-val is 0.01 (MACS2 internal default is 0.05)
    else
      m2r_args="$m2r_args --qvalue 0.01"
    fi
    s4m_log "Optimising for 'regular' peaks"
  fi

  ## Run MACS2 with appropriate args
  s4m_debug "Calling MACS2: [$MACS2_BIN $m2r_args]"; sleep 2
  $MACS2_BIN $m2r_args
  return $?
}


macs2_do_peak_calls () {
  ## Run MACS2 over each library (iterate over targets rows)
  fileutil_iterate_targets "$MACS2_targets" "macs2_run_target"
  ret=$?

  if [ $ret -eq 0 ]; then
    s4m_log "MACS2 completed successfully over all targets"
  fi

  ## Write a suitable sample table input file for ChIPQC now, otherwise it's
  ## a real pain to automate later.
  ## If users don't care about ChIPQC they can just ignore this output file!
  ##
  ## TODO: Maybe turn this into an optional flag, although it runs fast anyway.
  ##
  if [ $ret -eq 0 ]; then
    s4m_log "Writing sample input table for R/ChIPQC (ignore this if not needed)"
    fbase=`basename "$MACS2_targets" .txt`
    macs2_write_chipqc_samples_file "$MACS2_outputdir/${fbase}_chipqc.txt"
    ret=$?
  fi
  return $ret
}


## Run the peak model image plotting .R scripts created by MACS2 for each
## ChIP library
macs2_plot_peak_models () {
  s4m_log "Plotting per-library peak models and cross-correlation (fragment length prediction)"
  cd "$MACS2_outputdir"
  for model_r in *model.r
  do
    call_R $MACS2_R "$model_r"
  done
  cd $OLDPWD
}


## Validate ChIP specific fields of input targets file
macs2_validate_targets_file () {
  mvtf_targets="$1"
  head -1 "$mvtf_targets" | grep "\bReplicate\b" > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    warn="No 'Replicate' field in input targets file - this could prevent
  successful operation of R/ChIPQC over MACS2 results. If outputting ChIPQC-compatible sample
  table after peak calling, will attempt to add 'Replicate' values automatically."
    s4m_warn "$warn"
    sleep 2
  fi
}


## Call peaks using MACS2
callpeaks_handler () {

  MACS2_BIN=macs2
  if [ ! -z "$MACS2_macs2_bin" ]; then
    s4m_log "Using alternate MACS2 binary: $MACS2_macs2_bin"; sleep 2
    MACS2_BIN="$MACS2_macs2_bin"
  fi

  ## Validate minimum targets fields (does not check for ChIP-specific fields)
  fileutil_validate_targets_file "$MACS2_targets"
  ret=$?
  if [ $ret -ne 0 ]; then
    return $ret
  fi

  ## Check ChIP-specific targets fields
  macs2_validate_targets_file "$MACS2_targets"
  ret=$?
  if [ $ret -ne 0 ]; then
    return $ret
  fi

  macs2_do_peak_calls
  ret=$?
  if [ $ret -ne 0 ]; then
    return $ret
  fi
 
  macs2_plot_peak_models
  return $?
}


main () {
  ## Load "call_R()" function
  s4m_import "Rutil/Rutil.sh"

  ## Wrapper for "callpeak" command - handles "regular" and "broad" peak calls
  ## via "peaktype" option
  s4m_route callpeaks callpeaks_handler

}

### START ###
main

