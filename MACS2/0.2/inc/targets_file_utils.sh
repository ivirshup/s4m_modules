#!/bin/sh

## Validate targets file contains minimum required columns / attributes
##
fileutil_validate_targets_file () {
  fvtf_targets="$1"
  if [ ! -f "$fvtf_targets" ]; then
    s4m_error "Targets file '$fvtf_targets' not found!"
    return 1
  fi

  ## Check for required columns
  fvtf_required_cols="SampleID
SampleType
ReadFile1"
  for req in $fvtf_required_cols
  do
    head -1 "$fvtf_targets" | grep -P "\b$req\b" > /dev/null 2>&1
    if [ $? -ne 0 ]; then
      s4m_error "Targets file missing required column '$req'"
      return 1
    fi
  done

  ## SampleID must be first
  if [ `head -1 "$fvtf_targets" | cut -f 1` != "SampleID" ]; then
    s4m_error "First column in targets file must be 'SampleID'!"
    return 1
  fi

  ## Each row must have same number of fields
  fvtf_nc=`awk -F'\t' '{print NF}' "$fvtf_targets" | sort | uniq | wc -l`
  if [ $fvtf_nc -gt 1 ]; then
    s4m_error "Targets file must have same number of fields in all rows (i.e. TABs)!"
    return 1
  fi
}


## Get an attribute from a target row by numerical token position (1-based)
##
fileutil_extract_target_token () {
  fett_targetrow="$1"
  fett_attrpos="$2"
  echo "$fett_targetrow" | cut -f $fett_attrpos
}


## Get a field from a target row by its attribute (column) name
##
fileutil_extract_token_by_name () {
  fetbn_targetrow="$1"
  fetbn_attrmap="$2"
  fetbn_attrname="$3"
  ## if non-empty, taken as boolean true - disables error throwing on bad attribute name
  fetbn_ignorebadattr="$4"

  if [ -z "$fetbn_targetrow" -o -z "$fetbn_attrmap" -o -z "$fetbn_attrname" ]; then
    s4m_error "fileutil_extract_token_by_name(): Internal: One or more arguments missing!"
    return 1
  fi
  fetbn_attr_target=`echo "$fetbn_attrmap" | grep -P "\b$fetbn_attrname\b" 2> /dev/null`
  if [ $? -ne 0 ]; then
    if [ -z "$fetbn_ignorebadattr" ]; then
      s4m_error "Failed to extract token from targets (bad column name '$fetbn_attrname')"
      return 1
    else
      echo ""
      return
    fi
  fi
  fetbn_attrpos=`echo "$fetbn_attr_target" | cut -d':' -f 2`
  echo "$fetbn_attrpos" | grep -P "^[0-9]+$" > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    #s4m_debug "Attribute map = [$fetbn_attrmap]"
    s4m_error "fileutil_extract_token_by_name(): Internal: Attribute map returned non-numeric token position!"
    return 1
  fi

  fileutil_extract_target_token "$fetbn_targetrow" $fetbn_attrpos
}


## Return a <column>:<pos> multi-line string map of targets file attributes
## so we can get an attribute by its name from a targets file row
##
fileutil_get_targets_attr_map () {
  fgtam_targets="$1"
  if [ ! -f "$fgtam_targets" ]; then
    s4m_push_error "Targets file '$fgtam_targets' not found!"
    return 1
  fi
  head -1 "$fgtam_targets" | tr "\t" "\n" | grep -n "." | awk -F':' '{print $2":"$1}'
}


## Extract rows from targets file
##
fileutil_get_targets_rows () {
  fgtr_targets="$1"
  if [ ! -f "$fgtr_targets" ]; then
    s4m_push_error "Targets file '$fgtr_targets' not found!"
    return 1
  fi
  ## return 2nd line onwards
  tail -n +2 "$fgtr_targets"
}


## Run a user function over each row of targets file
##
fileutil_iterate_targets () {
  fit_targets="$1"
  fit_iterfunc="$2"

  fit_targets_attr_map=`fileutil_get_targets_attr_map "$fit_targets"`
  ret=$?
  if [ $ret -ne 0 ]; then
    s4m_pop_error
    return $ret
  fi

  fileutil_get_targets_rows "$fit_targets" | while read line
  do
    eval "$fit_iterfunc \"$line\" \"$fit_targets_attr_map\""
    ret=$?
    if [ $ret -ne 0 ]; then
      continue
    fi
  done
}

