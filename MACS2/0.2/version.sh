#!/bin/sh

# Include S4M functions
for i in `ls $S4M_SCRIPT_HOME/inc/*.sh`; do . $i; done

print_module_version
`macs2 --version`
