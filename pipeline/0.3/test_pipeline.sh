#!/bin/sh
## test_pipeline.sh: For "unittest" module execution
## To be executed inside module directory

## Set up test environment before any tests are run
oneTimeSetUp () {
  PIPELINE_TEST_DATADIR="./etc"
  export PIPELINE_TEST_DATADIR
 
  ## Load functions being tested
  . ./inc/pipeline_funcs.sh

  ## Set up temp data / files location and populate dummy files
  PIPELINE_TEST_TMP="$S4M_TMP/$$"
  export PIPELINE_TEST_TMP
  mkdir "$PIPELINE_TEST_TMP"
}

## Run after all tests finished
oneTimeTearDown () {
  rm -rf "$PIPELINE_TEST_TMP"
}

## Run between tests (NA)
#setUp () {
#}


### TESTS ###

testPipelineFileLoad () {
  ## Load from line 1 to end
  load_command_file "$PIPELINE_TEST_DATADIR"/test.pipeline 1 -  > /dev/null 2>&1
  assertTrue "$?"
  assertFalse "[ -z $S4M_PIPELINE_CMDS ]"
}

testPipelineFileLoadError () {
  ## Load from line 1 to end
  load_command_file "$PIPELINE_TEST_DATADIR"/test.does_not_exist 1 -  2> "$PIPELINE_TEST_TMP/err.log" > /dev/null
  grep -i "not found" "$PIPELINE_TEST_TMP/err.log"  > /dev/null 2>&1
  assertTrue "$?"
}

## Assumes something ran if there is an output log. Probably not the best test.
testPipelineRun () {
  ## Load from line 1 to end
  load_command_file "$PIPELINE_TEST_DATADIR"/test.pipeline 1 -  > /dev/null 2>&1
  run_commands > "$PIPELINE_TEST_TMP/out.log" 2>&1
  assertTrue "[ -s $PIPELINE_TEST_TMP/out.log ]"
}

testPipelineCaptureSTDERR () {
  run_commands 2> "$PIPELINE_TEST_TMP/err.log" > /dev/null
  assertTrue "[ -s $PIPELINE_TEST_TMP/err.log ]"
}

## NOTE: If etc/test.pipeline lines added or removed before line 9, this will obviously need updating!
testPipelineErrorLineReporting () {
  exitLineNumber=`grep -P "exited.*non-zero" "$PIPELINE_TEST_TMP/err.log" | sed -r -e 's|^.*on line ([0-9]+).*$|\1|'`
  assertEquals 9 $exitLineNumber
}

