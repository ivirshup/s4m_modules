#/bin/sh
##==========================================================================
## Module: pipeline
## Author: Othmar Korn
##
## Allows a user to provide a list of S4M commands (and their args) to be
## executed in sequential fashion. Pipeline may be a text file or passed
## via STDIN.
##==========================================================================

# Include S4M functions 
for i in `ls $S4M_SCRIPT_HOME/inc/*.sh`; do . $i; done

## ============================================================================

## Include subread utility functions
. ./inc/pipeline_funcs.sh


## Handler for "pipeline" command
pipeline_handler () {
  load_command_file "$commandfile_in" "$startline" "$endline" "$exclude_cmd_regex" "$keep_header" 
  ret=$?
  if [ $ret -ne 0 ]; then
    return $ret
  fi
  if [ ! -z "$commandfile_out" ]; then
    save_command_file "$commandfile_out"
    if [ $? -ne 0 ]; then
      ## Not a fatal error, don't exit
      s4m_warn "Skipped writing post-parsed pipeline file."
    fi
  fi
  if [ ! -z "$dryrun" -o "$dryrun" = "true" ]; then
    s4m_log "Dry-run mode ENABLED, no commands will be executed."
  else
    ## NOTE: Capture of STDOUT broken for nested s4m output - disabling for now.
    #run_commands "$stdoutlog" "$stderrlog"
    run_commands
    ret=$?
    if [ $ret -ne 0 ]; then
      return $ret
    fi
  fi
}


main () {
  commandfile_in="$pipeline_commandfile"
  ## File to write post-parsed pipeline file. If not given, it is not written.
  commandfile_out="$pipeline_outfile"
  
  ## NOTE: Capture of STDOUT broken for nested s4m output - disabling for now.
  #stdoutlog="$pipeline_stdout"
  #stderrlog="$pipeline_stderr"

  dryrun="$pipeline_dryrun"

  ## Start and End filter
  startline="$pipeline_startat"
  if [ $? -ne 0 -o -z "$startline" ]; then
    startline=1
  fi
  endline="$pipeline_endat"
  if [ $? -ne 0 -o -z "$endline" ]; then
    endline="-"
  fi

  ## Regex exclude filter
  exclude_cmd_regex="$pipeline_exclude"
  if [ "$exclude_cmd_regex" = "true" ]; then
    exclude_cmd_regex=""
  fi

  ## Keep all before module commands? (Overrides regex exclusion)
  keep_header="$pipeline_keepheader"

  ## Execute pipeline
  s4m_route "pipeline" "pipeline_handler"
}

### START ###
main

