
Test read library
=================

read1.fq
read2.fq:
	Simulated 100bp paired end reads * 100 sequences made using "wgsim" (samtools suite)
	extracted from Ensembl v69 human reference DNA sequence.

	$ wgsim -N 100 -1 100 -2 100 -S `date+"%s"` GRCh37.69.fa read1.fq read2.fq
