# Module name (no spaces, alphanumeric + underscore only)
Name=subread
# Module command namespace. Convention: Short single word related to module name, all lower case
Namespace=subread
# Module version string
Version=0.12
# Optional version history. Format: <version>:<date>:<changes>
# Can wrap over multiple lines
VersionHistory=0.12:20170413:fastq2bam now has "-T", "-f1" and "-f2" flags for DNA alignment,
0.11:20170403:subreadindex now can build color-space indexes, 
0.10:20170308:subreadindex now has "-F" toggle to allow building full index (small-RNAseq etc),
0.9:20170222:Now correctly able to specify target GTF feature when using "custom",
0.8:20170204:New phred offset, non-unique, nbestlocations and nconsensus options,
0.7:20161027:Fix overlay density plot colours / labelling,
0.6:20160812:New 'features' (-f) flag for annotatebams and generic GTF annotation,
0.5:20160801:subreadindex target directory now created if not existing,
0.4:20160429:Using new 'call_R()' API from Rutil module,
0.3:20160114:Fix broken fastqc parallel logic causing hang and increased poll delay,
0.2:20151117:Added max mismatches and indels flags,
0.1:20150316:Subread feature counts now with "ignoreDup" set to TRUE,
0.1a:Initial
# Module author
Author=Othmar Korn
# Module is enabled (1 or 0)
Enabled=1
# Module description (one line, no wrapping)
Description=RNASeq processing pipeline

# Comma separated list of <command>:<script_file_relative_to_module_directory>.
# Can wrap over multiple lines
CommandRoutes=fastqc:subread.sh,
fastq2bam:subread.sh,
subreadindex:subread.sh,
annotatebams:subread.sh,
qc:subread.sh

# Comma separated list of <command>:[<opt_label>]:<opt_description>:<OPTIONAL|MANDATORY>:[<opt_flag>]
# Can wrap over multiple lines
CommandOptions=fastqc:fastqdir:Input directory containing FASTQ files:MANDATORY:i,
fastqc:outdir:Output directory for QC report:MANDATORY:o,
fastqc:targets:Use only FASTQ targets provided (bioinf.wehi.edu.au/RNAseqCaseStudy/ ):OPTIONAL:t,
fastqc:ncpu:If given, use this many processes (equal or greater than # FASTQ files):OPTIONAL:N,
fastqc:dryrun:Toggle 'dry-run' mode, don't execute any sub-utility functions:OPTIONAL:n,
fastqc:updatemode:If flag specified, only run if input FASTQ newer than existing QC reports:OPTIONAL:u,
fastq2bam:fastqdir:Input directory containing FASTQ files:MANDATORY:i,
fastq2bam:targets:Path to read targets file (bioinf.wehi.edu.au/RNAseqCaseStudy/ ):MANDATORY:t,
fastq2bam:outdir:Output directory for aligned BAMs and related outputs:MANDATORY:o,
fastq2bam:updatemode:If flag specified, only run if input FASTQ newer than output BAMs:OPTIONAL:u,
fastq2bam:indexdir:Subread genome index files directory:MANDATORY:x,
fastq2bam:indexname:If multiple genome indexes in indexdir, supply basename for target:OPTIONAL:n,
fastq2bam:indexspace:Reads in 'base' or 'color' space? Default is 'base':OPTIONAL:s,
fastq2bam:genome:Path to target genome FASTA (* required if index not built yet!):OPTIONAL:g,
fastq2bam:type:Aligment type - RNA or DNA? Default is "RNA":OPTIONAL:T,
fastq2bam:fraglen1:Mininum input fragment length for DNA alignment. Default 50.:OPTIONAL:f1,
fastq2bam:fraglen2:Maximum input fragment length for DNA alignment. Default 600.:OPTIONAL:f2,
fastq2bam:mismatches:Max mismatches (SNPs) to allow in sequence alignment. Default is '5':OPTIONAL:m,
fastq2bam:indels:Max indels to allow in sequence alignment. Default is '5':OPTIONAL:I,
fastq2bam:multimap:Report multi-mapping (non-uniquely mapping) reads. Default is false:OPTIONAL:M,
fastq2bam:nbestlocations:Number of equal best locations for multi-map reads. Default is 1:OPTIONAL:b,
fastq2bam:nconsensus:Number of consensus subreads required. See R/Subread docs. Default is 10:OPTIONAL:c,
fastq2bam:phredoffset:Phred score offset. Default is '33':OPTIONAL:P,
fastq2bam:ncpu:Number of CPU threads for alignment. Default is 2:OPTIONAL:N,
fastq2bam:R:Alternate R binary path if not global 'R':OPTIONAL:R,
subreadindex:indexdir:Subread genome index files directory (created if not existing):MANDATORY:x,
subreadindex:indexname:Basename of index file to create or load from index directory:MANDATORY:n,
subreadindex:indexspace:Reads in 'base' or 'color' space? Default is 'base':OPTIONAL:s,
subreadindex:genome:Path to reference genome FASTA:MANDATORY:g,
subreadindex:full:Toggle FULL (ungapped) index build. Do no specify this flag if default gapped is required:OPTIONAL:F,
subreadindex:R:Alternate R binary path if not global 'R':OPTIONAL:R,
annotatebams:inputdir:Directory of SAM or BAM files to annotate against GTF file:MANDATORY:i,
annotatebams:outdir:Output directory for annotated count tables:MANDATORY:o,
annotatebams:gtf:Input GTF file for annotation:MANDATORY:g,
annotatebams:targets:Path to read targets file (bioinf.wehi.edu.au/RNAseqCaseStudy/ ):MANDATORY:t,
annotatebams:features:Which feature types (comma separated) to annotate? Choices> "gene","transcript" or user specified. Default "gene,transcript":OPTIONAL:f,
annotatebams:multimap:Toggle (fractional 1/n) counting of multi-mapping reads. Default is false:OPTIONAL:M,
annotatebams:ncpu:Number of threads for feature count annotation:OPTIONAL:N,
annotatebams:updatemode:If flag specified, only run if there are SAM/BAM files newer than annotation output:OPTIONAL:u,
annotatebams:R:Alternate R binary path if not global 'R':OPTIONAL:R,
qc:input:Table of input read counts for QC plots:MANDATORY:i,
qc:output:Output directory for QC plots and data:MANDATORY:o,
qc:targets:Path to read targets file (bioinf.wehi.edu.au/RNAseqCaseStudy/ ):MANDATORY:t,
qc:qcname:Short description used to build title and file names of output QC plots:OPTIONAL:n,
qc:R:Alternate R binary path if not global 'R':OPTIONAL:R

# Comma separated binary dependencies in format <command>:<binary_or_command_name_or_path>[,...]
# Can wrap over multiple lines
CommandDepends=fastqc:fastx_quality_stats,
fastqc:fastq_quality_boxplot_graph.sh,
fastqc:fastx_nucleotide_distribution_graph.sh,
fastqc:gnuplot,
fastqc:convert

# Module dependencies, comma separated. Format:  <module_name>[==|>=<module_version>],...
# The module version and qualifier parts are optional.
# Can wrap over multiple lines.
# Example: pipeline>=0.1,foo>=2.1,bar
# Example: pipeline,foo,bar>=3.3
ModuleDepends=Rutil>=0.2
