#/bin/sh
##===========================================================================
## Module: unittest
## Author: Othmar Korn
##
## A wrapper for "shUnit2" https://code.google.com/p/shunit2/
## shUnit2 (C) Kate Ward, 2008 and is licensed under the LGPL.
##
## See: ./include/shunit2*
##===========================================================================

# Include S4M functions 
for i in `ls $S4M_SCRIPT_HOME/inc/*.sh`; do . $i; done

##===========================================================================

## Version of shUnit2 bundled with this module
SHUNIT2_LIB="shunit2-2.1.6"

## A unit test script client can call this to return value for
## an argument that was passed using the "--args" argument
unittest_get_arg () {
  uga_target="$1"
  shift

  #echo "DEBUG: unittest_get_arg(): UNNITEST_TARGET_ARGS=[$UNITTEST_TARGET_ARGS]" 1>&2

  ## Locally replace default array with user args so we can process
  set -- "$UNITTEST_TARGET_ARGS"

  while [ ! -z "$1" ];
  do
    ## remove leading dashes, if any
    ## NOTE: In current s4m v0.5 (2016-08-23), the arg will never have
    ##  leading dashes because s4m would have interpreted it as a module
    ##  command arg for 'unittest' module
    arg=`echo "$1" | sed -r -e 's|^\-+||'`
    key=`echo "$arg" | cut -d'=' -f 1`
    val=`echo "$arg" | cut -d'=' -f 2`

    if [ "$key" = "$uga_target" ]; then
      echo "$val"
      return
    fi
    shift
  done
}


## Find scripts named "test_*.sh" in target module path
find_module_tests () {
  ## NOTE: If user input module name matches a relative path (due to having
  ## been run in modules subdirectory of s4m, for example), then the module
  ## name will have been replaced by a full absolute path - to undo this, we
  ## use 'basename'
  targetmodule=`basename "$1"`
  if [ ! -d "$S4M_MODULE_PATH/$targetmodule" ]; then
    s4m_fatal_error "Target module '$targetmodule' not found in modules path '$S4M_MODULE_PATH'!"
  fi

  modulevers=`s4m_get_max_module_version "$S4M_MODULE_PATH/$targetmodule"`
  testscripts=`cd $S4M_MODULE_PATH/$targetmodule/$modulevers && ls -1 test_*.sh 2> /dev/null` 
  echo $testscripts
}


## Run shUnit2 on each test shell script found in target module path
##
run_shunit2_over_test_scripts () {
  targetmodpath="$1"
  ## TODO: T#1537: Test
  targetargs="$2"
  shift; shift
  testscripts=$@

  ## Make available target args to test script
  ## TODO: T#1537: Test
  if [ ! -z "$targetargs" ]; then
    UNITTEST_TARGET_ARGS="$targetargs"
    export UNITTEST_TARGET_ARGS
  fi

  ## change into target module path as any test scripts there are written
  ## relative to that module's path
  UNITTEST_OLD_PWD=`pwd`
  export UNITTEST_OLD_PWD
  cd "$targetmodpath"

  for testfile in $testscripts
  do
    s4m_log "Running test script [$testfile].."
    
    . "$S4M_MODULE_PATH/$S4M_MODULE/$S4M_MOD_VERS/include/$SHUNIT2_LIB/src/shunit2" "$testfile"

    s4m_log "  done"
  done

  ## change back to this module path
  cd "$UNITTEST_OLD_PWD"
}


## Handler for "unittest" command`
##
unittest_handler () {
  targetmodule="$unittest_module"
  ## TODO: T#1537: Test
  targetargs="$unittest_args"
  s4m_debug "unittest_args=[$unittest_args]"

  ## Get test_*.sh files in default (latest) module version
  testscripts=`find_module_tests "$targetmodule"`
  ret=$?
  if [ $ret -ne 0 ]; then
    exit $ret
  fi
  ## Test scripts can use this variable to find out which module they belong to
  UNITTEST_TARGET_MODULE="$targetmodule"
  export UNITTEST_TARGET_MODULE

  if [ -z "$testscripts" ]; then
    s4m_fatal_error "Failed to find any unit test scripts in target module '$targetmodule' directory!"
  else
    s4m_debug "Found test scripts: [$testscripts]"
  fi

  modulevers=`s4m_get_max_module_version "$S4M_MODULE_PATH/$targetmodule"`
  run_shunit2_over_test_scripts "$S4M_MODULE_PATH/$targetmodule/$modulevers" "$targetargs" $testscripts
}


main () {
  s4m_route "unittest" "unittest_handler"
}

### START ###
main

