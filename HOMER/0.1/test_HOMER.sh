#!/bin/sh
## test_HOMER.sh: For "unittest" module execution
## To be executed inside module directory

## Set up test environment before any tests are run
oneTimeSetUp () {
  ## Load functions being tested
  test -f ./inc/HOMER_funcs.sh && . ./inc/HOMER_funcs.sh

  ## Set up temp data / files location and populate dummy files
  mkdir "$S4M_TMP/$$"
   
}

## Run after all tests finished
oneTimeTearDown () {
  rm -rf "$S4M_TMP/$$"
}

## Run between tests
setUp () {
  return
}



## TESTS ##

## Test that required binary dependencies exist.
## We're not reinventing the wheel here, so simply call the same core s4m
## function for dependency validation that would be run whenever the target
## module is invoked.
testBinaryDependencies () {
  s4m_validate_module_binary_dependencies "$UNITTEST_TARGET_MODULE"
  assertEquals 0 $?
}

testPeakFileListParsing () {
  ## comma-separated input list
  parsed=`annotatepeaks_parse_list "file.one,file.two"`
  assertEquals "$parsed" "file.one
file.two"

  ## new-line separated input list
  parsed=`annotatepeaks_parse_list "file.one
file.two"`
  assertEquals "$parsed" "file.one
file.two"

  ## space-separated input list
  parsed=`annotatepeaks_parse_list "file.one file.two"`
  assertEquals "$parsed" "file.one
file.two"
}


