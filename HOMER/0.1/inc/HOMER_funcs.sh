#!/bin/sh

## Parse a space,newline or comma separated list of file names into a 
## newline separated list.
##
## Unit Test:
##   test_HOMER.sh:testPeakFileListParsing()
##
annotatepeaks_parse_list () {
  apl_input="$1"
  if echo "$apl_input" | grep "," > /dev/null; then
    echo "$apl_input" | tr "," "\n"
  else
    echo "$apl_input" | tr " " "\n"
  fi
}


