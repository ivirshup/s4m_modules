## CPUs per job
NCPU=%NCPU%
R_BIN="%R_BIN%"
R2_BIN="%R2_BIN%"

fastqdir="%PROCESSED_DIR%/../raw"
outdir="%PROCESSED_DIR%"
erccout="$outdir/ERCC"
erccindex="$erccout/subread_index_ERCC"
erccbamout="$erccout/aligned"
ercccountout="$erccout/counts"
erccqcout="$erccout/qc"

targets="$fastqdir/targets.txt"
ERCCfa="$fastqdir/ERCC.fa"
ERCCgtf="$fastqdir/ERCC.gtf"
## ERCC spike-in molecule count table
ERCCmolecules="$fastqdir/ERCC_molecules.txt"

### =========================================================================

## Build ERCC index (if needed)
if [ -f "$ERCCfa" ]; then \
  mkdir -p "$erccindex" "$erccbamout" "$ercccountout" "$erccqcout" ;\
  s4m subread::subreadindex -x "$erccindex" -n ERCC -g "$ERCCfa" -R $R_BIN ;\
  ## subread alignment to ERCC (update-only mode)
  s4m subread::fastq2bam -i "$fastqdir" -o "$erccbamout" -t "$targets" -x "$erccindex" -u -N $NCPU -R $R_BIN ;\
  ## ERCC read alignment stats and report using generic mapping stats module.
  ## NOTE: Barplot shows a maximum of 50 samples by default
  s4m samtools::mapstats -i "$erccbamout" -o "$erccbamout" --ymax ROUND --stacked FALSE --title "ERCC_Reads_Mapped_(%)" -R $R_BIN ;\
  ## TODO: T#2513: Test on production dataset
  ## Custom ERCC mapping stats boxplot - all samples
  s4m ercc_tools::ercc_mapstats -i "$erccbamout/mapstats.txt" -o "$erccbamout" -R $R_BIN ;\
  ## ERCC annotation, count summarisation (update-only mode)
  s4m subread::annotatebams -i "$erccbamout" -o "$ercccountout" -g "$ERCCgtf" -t "$targets" -f custom -u -N $NCPU -R $R_BIN ;\
  if [ -f "$ERCCmolecules" ]; then \
    ## Custom ERCC input molecules vs output counts correlation (ERCC recovery) QC
    s4m ercc_tools::ercc_recovery -m "$ERCCmolecules" -c "$ercccountout/gene_count_frags.txt" -o "$erccqcout" -R $R_BIN ;\
  fi ;\
fi
