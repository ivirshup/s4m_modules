#/bin/sh
##==========================================================================
## Module: s4m
## Author: Othmar Korn
##
## Stemformatics specific functions and pipelines
##==========================================================================

# Include S4M functions 
for i in `ls $S4M_SCRIPT_HOME/inc/*.sh`; do . $i; done

##==========================================================================

. inc/setup.sh
. inc/stemformatics_funcs.sh


##
## Handler for "chipseq-all" command
##
chipseq_handler () {
  datasetdir="$stemformatics_datasetdir"
  finalise="$stemformatics_finalise"
  validateonly="$stemformatics_validate"
  dryrun="$stemformatics_dryrun"
  rerunmode="$stemformatics_rerun"
  continuemode="$stemformatics_continue"

  replacemode="false"
  if [ ! -z "$continuemode" ]; then
    replacemode="true"
  fi
  ## Finalise only?
  if [ ! -z "$finalise" ]; then
    finalise_s4m_chipseq_dataset "$datasetdir" "$genome"
    return $?
  fi

  if ! prevalidate_s4m_chipseq_dataset "$datasetdir"; then
    s4m_error "Failed ChIP-seq dataset pre-validation, exiting.."
    return 1
  fi
  if [ "$validateonly" = "true" ]; then
    s4m_log "Validation complete, exiting"
    return
  fi

  ## Run pipeline
  existing=`chipseq_dataset_has_outputs "$datasetdir"`
  if [ "$existing" = "true" ]; then
    s4m_log "Detected previous outputs for dataset.."
  else
    s4m_log "No previous outputs detected for dataset.."
  fi
  if [ "$existing" = "true" -a -z "$rerunmode" -a -z "$continuemode" ]; then
    s4m_error "Please specify relevant flag to continue with existing, or start a new run."
    return 1
  else
    if [ $replacemode = "true" ]; then
      s4m_log "Replacing / continuing existing run.."
    else
      s4m_log "Starting a new run.."
    fi

    run_chipseq_pipeline_on_dataset
    ret=$?
    if [ $ret -ne 0 ]; then
      return $ret
    else
      if [ "$dryrun" != "true" ]; then
        finalise_s4m_chipseq_dataset "$datasetdir" "$genome"
        return $?
      fi
    fi
  fi
}


##
## Handler for "small-rnaseq-all" command
##
## NOTE: Largely a copy of "rnaseq_handler()" - it seemed better to do this for clarity
##       (separation of concerns), rather than confusing the default rnaseq_handler() 
##       with stuff that's specific to the small RNAseq pipeline. Can revisit if needed.
##
small_rnaseq_handler () { 
  datasetdir="$stemformatics_datasetdir"
  genome="$stemformatics_genome"
  finalise="$stemformatics_finalise"
  ## NOTE: Default count table target if not given is "gene_count_frags_CPM_log2.txt"
  counttable="$stemformatics_counttable"
  validateonly="$stemformatics_validate"
  rerunmode="$stemformatics_rerun"
  continuemode="$stemformatics_continue"
  phredoffset="$stemformatics_phredoffset"

  ## Small-RNASeq pipeline uses different finalised count table to standard RNAseq pipeline
  if [ -z "$counttable" ]; then
    counttable="gene_count_frags_CPM_log2.txt"
  fi
  replacemode="false"
  if [ ! -z "$continuemode" ]; then
    replacemode="true"
  fi
  ## Finalise only?
  if [ ! -z "$finalise" ]; then
    finalise_s4m_rnaseq_dataset "$datasetdir" "$genome" "$counttable"
    return $?
  fi

  if ! prevalidate_s4m_rnaseq_dataset "$datasetdir"; then
    s4m_error "Failed small-RNAseq dataset pre-validation, exiting.."
    return 1
  fi
  if [ "$validateonly" = "true" ]; then
    s4m_log "Validation complete, exiting"
    return
  fi

  ## Run pipeline
  existing=`rnaseq_dataset_has_outputs "$datasetdir"`
  if [ "$existing" = "true" ]; then
    s4m_log "Detected previous outputs for dataset.."
  else
    s4m_log "No previous outputs detected for dataset.."
  fi
  if [ "$existing" = "true" -a -z "$rerunmode" -a -z "$continuemode" ]; then
    s4m_error "Please specify relevant flag to continue with existing, or start a new run."
    return 1
  else
    if [ $replacemode = "true" ]; then
      s4m_log "Replacing / continuing existing run.."
    else
      s4m_log "Starting a new run.."
    fi

    ## Just like standard RNAseq pipeline except we override the pipeline template for small-RNAseq
    run_rnaseq_pipeline_on_dataset "$datasetdir" "$genome" "$ncpu" "$phredoffset" "$replacemode" "$s4m_R" "$s4m_R2" "$dryrun" "SMALL_RNASEQ_PIPELINE_TEMPLATE"
    ret=$?
    if [ $ret -ne 0 ]; then
      return $ret
    else
      if [ "$dryrun" != "true" ]; then
        finalise_s4m_rnaseq_dataset "$datasetdir" "$genome" "$counttable"
        return $?
      fi
    fi
  fi
}


##
## Handler for "rnaseq-all" command
##
rnaseq_handler () {
  datasetdir="$stemformatics_datasetdir"
  genome="$stemformatics_genome"
  finalise="$stemformatics_finalise"
  ## NOTE: Default count table target name in $FINAL_COUNT_TABLE (inc/stemformatics_funcs.sh)
  counttable="$stemformatics_counttable"
  validateonly="$stemformatics_validate"
  rerunmode="$stemformatics_rerun"
  continuemode="$stemformatics_continue"
  phredoffset="$stemformatics_phredoffset"

  replacemode="false"
  if [ ! -z "$continuemode" ]; then
    replacemode="true"
  fi
  ## Finalise only?
  if [ ! -z "$finalise" ]; then
    finalise_s4m_rnaseq_dataset "$datasetdir" "$genome" "$counttable"
    return $?
  fi

  if ! prevalidate_s4m_rnaseq_dataset "$datasetdir"; then
    s4m_error "Failed RNAseq dataset pre-validation, exiting.."
    return 1
  fi
  if [ "$validateonly" = "true" ]; then
    s4m_log "Validation complete, exiting"
    return
  fi

  ## Run pipeline
  existing=`rnaseq_dataset_has_outputs "$datasetdir"`
  if [ "$existing" = "true" ]; then
    s4m_log "Detected previous outputs for dataset.."
  else
    s4m_log "No previous outputs detected for dataset.."
  fi
  if [ "$existing" = "true" -a -z "$rerunmode" -a -z "$continuemode" ]; then
    s4m_error "Please specify relevant flag to continue with existing, or start a new run."
    return 1
  else
    if [ $replacemode = "true" ]; then
      s4m_log "Replacing / continuing existing run.."
    else
      s4m_log "Starting a new run.."
    fi

    run_rnaseq_pipeline_on_dataset "$datasetdir" "$genome" "$ncpu" "$phredoffset" "$replacemode" "$s4m_R" "$s4m_R2" "$dryrun"
    ret=$?
    if [ $ret -ne 0 ]; then
      return $ret
    else
      if [ "$dryrun" != "true" ]; then
        finalise_s4m_rnaseq_dataset "$datasetdir" "$genome" "$counttable"
        return $?
      fi
    fi
  fi
}


##
## Handler for "align-reads" command
##
alignreads_handler () {
  inputdir="$stemformatics_inputdir"
  outputdir="$stemformatics_outputdir"
  targets="$stemformatics_targets"

  stemformatics_align_fastq "$inputdir" "$outputdir" "$targets" "$ncpu" "$s4m_R" "$s4m_R2"
  return $?
}


##
## Handler for "qcpdf" command
##
qcpdf_handler () {
  procdir="$stemformatics_procdir"

  stemformatics_compile_qc_pdf "$procdir"
  return $?
}


##
## Handler for "fastqc" command
##
fastqc_handler () {
  dsdir="$stemformatics_datasetdir"
  ncpu="$stemformatics_ncpu"

  ## Default NCPU=2 if not specified
  if [ -z "$ncpu" ]; then
    ncpu=2
  fi

  timestamp=`date +"%Y%m%d-%H%M%S"`
  procdir="processed.fastqc_only.$timestamp"
  outputdir="$dsdir/source/$procdir"
  mkdir -p "$outputdir"

  instance_template=`get_instance_template "$FASTQC_ONLY_PIPELINE_TEMPLATE" \
    %DATASET_DIR%="$dsdir" %PROCESSED_OUTDIR%="$procdir" %NCPU%=$ncpu`

  ## Write instance pipeline file
  template_basename=`basename "$FASTQC_ONLY_PIPELINE_TEMPLATE"`
  echo "$instance_template" > "$outputdir/$template_basename"

  s4m_exec pipeline::pipeline -f "$outputdir/$template_basename"
  return $?
}


##
## Handler for "erccqc" command
##
erccqc_handler () {
  procdir="$stemformatics_procdir"
  ncpu="$stemformatics_ncpu"

  ## Default NCPU=2 if not specified
  if [ -z "$ncpu" ]; then
    ncpu=2
  fi

  ## Handle R overrides
  user_R=R
  if [ ! -z "$s4m_R" ]; then
    user_R="$s4m_R"
  fi
  user_R2=R
  if [ ! -z "$s4m_R2" ]; then
    user_R2="$s4m_R2"
  fi

  if [ ! -d "$procdir" ]; then
    s4m_error "Processed data path '$procdir' does not exist! Please check path is valid."
    return 1
  fi
  if [ ! -w "$procdir" ]; then
    s4m_error "Cannot write to processed data path '$procdir'! Please check permissions."
    return 1
  fi

  instance_template=`get_instance_template "$ERCCQC_ONLY_PIPELINE_TEMPLATE" \
    %PROCESSED_DIR%="$procdir" %NCPU%=$ncpu  %R_BIN%="$user_R" %R2_BIN%="$user_R2"`

  ## Write instance pipeline file
  template_basename=`basename "$ERCCQC_ONLY_PIPELINE_TEMPLATE"`
  echo "$instance_template" > "$procdir/$template_basename"

  s4m_exec pipeline::pipeline -f "$procdir/$template_basename"
  return $?
}


##
## Handler for "ercc-all" command
##
ercc_all_handler () {
  procdir="$stemformatics_procdir"
  ncpu="$stemformatics_ncpu"

  ## Default NCPU=2 if not specified
  if [ -z "$ncpu" ]; then
    ncpu=2
  fi

  ## Handle R overrides
  user_R=R
  if [ ! -z "$s4m_R" ]; then
    user_R="$s4m_R"
  fi
  user_R2=R
  if [ ! -z "$s4m_R2" ]; then
    user_R2="$s4m_R2"
  fi

  if [ ! -d "$procdir" ]; then
    s4m_error "Processed data path '$procdir' does not exist! Please check path is valid."
    return 1
  fi
  if [ ! -w "$procdir" ]; then
    s4m_error "Cannot write to processed data path '$procdir'! Please check permissions."
    return 1
  fi

  instance_template=`get_instance_template "$ERCC_ALL_PIPELINE_TEMPLATE" \
    %PROCESSED_DIR%="$procdir" %NCPU%=$ncpu  %R_BIN%="$user_R" %R2_BIN%="$user_R2"`

  ## Write instance pipeline file
  template_basename=`basename "$ERCC_ALL_PIPELINE_TEMPLATE"`
  echo "$instance_template" > "$procdir/$template_basename"

  s4m_exec pipeline::pipeline -f "$procdir/$template_basename"
  return $?
}


##
## Handler for "rnaqc" command
##
rnaqc_handler () {
  inputfile="$stemformatics_input"
  outdir="$stemformatics_outputdir"
  gtf="$stemformatics_gtf"
  targets="$stemformatics_targets"

  ## Load "call_R()" function
  s4m_import "Rutil/Rutil.sh"

  mkdir -p "$outdir"
  if [ $? -ne 0 ]; then
    s4m_error "Failed to create output path '$outdir'!"
    return 1
  fi

  do_rnaqc "$inputfile" "$outdir" "$gtf" "$targets"
  return $?
}


##
## Handler for "multimapqc" command
##
multimapqc_handler () {
  inputdir="$stemformatics_inputdir"
  outdir="$stemformatics_outputdir"
  ncpu="$stemformatics_ncpu"

  ## Load "call_R()" function
  s4m_import "Rutil/Rutil.sh"

  mkdir -p "$outdir"
  if [ $? -ne 0 ]; then
    s4m_error "Failed to create output path '$outdir'!"
    return 1
  fi

  do_bam_multimapqc "$inputdir" "$outdir" "$ncpu"
  return $?
}


main () {
  ncpu="$stemformatics_ncpu"
  dryrun="$stemformatics_dryrun"
  s4m_R="$stemformatics_R"
  s4m_R2="$stemformatics_R2"

  ## RNAseq suites
  s4m_route "rnaseq-all" "rnaseq_handler"
  s4m_route "small-rnaseq-all" "small_rnaseq_handler"
  ## Align reads only
  s4m_route "align-reads" "alignreads_handler"
  ## ChIP-seq
  s4m_route "chipseq-all" "chipseq_handler"
  ## Regenerate QC report PDF only
  s4m_route "qcpdf" "qcpdf_handler"
  ## Run only FASTQC over sequence libraries
  s4m_route "fastqc" "fastqc_handler"
  ## Run only ERCC QC
  s4m_route "erccqc" "erccqc_handler"
  ## Run full ERCC pipeline
  s4m_route "ercc-all" "ercc_all_handler"
  ## RNA composition QC
  s4m_route "rnaqc" "rnaqc_handler"
  ## Multimap read QC (on BAM files)
  s4m_route "multimapqc" "multimapqc_handler"
}

### START ###
main

