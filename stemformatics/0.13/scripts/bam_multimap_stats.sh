#!/bin/sh
## ============================================================================
## Script:	bam_multi_map_stats.sh
##
## Description:	Report multi-map stats of secondary alignments in BAM file.
##		Expects CIGAR field "NH:i:<N>" to track multimapping.
##		(e.g. as produced by R/Subread).
##
##		Pretty fast due to a single pass through the BAM file.
##
##		NOTE: Requires 'samtools' binary in caller's environment.
##
## Author:	O.Korn
## Created:	2017-02-10		
## Updated:	2017-02-27
## ============================================================================
bam="$1"
outdir="$2"
if [ -z "$outdir" ]; then
  outdir=`pwd`
fi
max_multimap="$3"
if [ -z "$3" ]; then
  max_multimap=50
fi

Usage () {
  echo "
Usage:  $0 <bamfile> <stats_output_dir> [<max_multimap=50>]

NOTE: Requires 'samtools' in environment (\$PATH)

"
}

if [ -z "$bam" ]; then
  Usage; exit
fi
if [ ! -f "$bam" ]; then
  echo "Error: Input BAM '$bam' not found!" 1>&2
  Usage
  exit 1
fi
which samtools > /dev/null 2>&1
if [ $? -ne 0 ]; then
  echo "Error: Failed to find dependency 'samtools'!" 1>&2
  exit 1
fi

bamfbase=`basename "$bam" .bam`
outputfile="$outdir/${bamfbase}.multimap_stats.txt"

echo "Input BAM:  $bam"
bamreads=`samtools view -F 4 $bam | wc -l`
echo "Aligned # reads: $bamreads"
echo "Writing to: $outputfile"
sleep 3

## This awk script iterates over all aligned, multi-mapping reads from input BAM
## file (using samtools to dump ASCII) to gather multi-map stats.
##
## Iterates over each aligned record and extracts the multi-map rate from the "NH"
## CIGAR string, incrementing the counter for the multi-map value by one. Only multi-mapping
## rates up to a maximum value is used (MAX_MULTI input to awk).
##
## When all reads are processed, we finally print the multi-mapping rates.
##
## Additionally, print total number of aligned reads that are multi-mapping vs unique.
## NOTE: The input 'bamreads' parameter corresponds to the total number of aligned reads 
##       which is used to calculate the multi-mapping vs unique percentages.
##
time samtools view -F 4 -f 256 $bam | awk -v MAX_MULTI=$max_multimap -v bamreads=$bamreads -v outputfile="$outputfile" 'BEGIN {
  ORS=""
  ## Set total BAM reads from input arg
  _bamreads = bamreads
  ## init multimap stats array
  for (i=1; i<=MAX_MULTI; i++) {
    _multimap[i] = 0
  }
  ## BAM metadata field number containing "NH:i:<N>"
  NH_pos = 0
  FATAL_ERROR = 0
}
NR==1 {
  ## work out which field contains "NH:i:<N>" flag
  for (i=1; i<=NF; i++) {
    if ($i ~ /NH:i:/) {
      NH_pos=i
      break
    }
  }
  if (NH_pos == 0) {
    print "Error: Failed to determine CIGAR \"NH\" string field position (looking for \"NH:i:<N>\")!" | "cat 1>&2"
    FATAL_ERROR = 1
    ## NOTE: Does not exit awk, this drops through to the END section first
    exit 1
  }
}
NR >= 1 {
  ## Extract <N> from  "NH:i:<N>"
  split($NH_pos, tokens, ":")
  NH_val = tokens[3]

  if (NH_val in _multimap) {
    _multimap[NH_val] += 1
  } else {
    _multimap[NH_val] = 1
  }
}
END {
  if (FATAL_ERROR > 0) {
    exit FATAL_ERROR
  }
  print "\nMulti-mapping alignment stats:\n\n"
  print "Multimap Size\tFrequency\n"
  ## Collate multi-mapping read stats
  print "Multimap Size\tFrequency\n" > outputfile
  multireads = 0
  for (i=2; i<=MAX_MULTI; i++) {
    multireads += _multimap[i]
    print " " i ":\t\t" _multimap[i] "\n"
    ## output row to table
    print i "\t" _multimap[i] "\n" >> outputfile
  }
  print "\nWrote multi-map frequency to file: " outputfile "\n\n"

  pct = multireads / _bamreads * 100
  print "Total multi-mapping reads:\t" multireads " ("
  printf("%0.3f", pct)
  print "%)\n"

  ## Uniquely mapping read stats
  uniqreads = _bamreads - multireads
  pct = uniqreads / _bamreads * 100
  print "Uniquely mapping reads:\t\t" uniqreads " ("
  printf("%0.3f", pct)
  print "%)\n\n"
}'

