#!/bin/bash
## Recursively join a bunch of TAB-separated files by IDs in first column.
## Files can have any identifiers (do not need to be equal length either).
##
## Resulting file contains superset of ALL identifiers encountered, with
## columns inserted as per '<pad_string' supplied.
##
## NOTE: Does not check for uniformity of column counts in input files.
##       Unexpected behaviour may occur.
##
## ============================================================================
## 
## Version 0.3  2016/01/08  OK
## - Added "--tmpdir" flag to override default /tmp usage
## - Various bug fixes
##
## Version 0.2  2014/02/06  OK
## - Added "--noheaders" option to ignore file header.
##   By default, assumes input files have headers.
##
## Version 0.1  2014/01/30  OK
## - First cut
##

## Value to insert for a field when not existing for a new merge
## e.g. "0" or "NA" or blank "" are some examples that might make sense.
pad_string="$1"
outfile="$$"

## Set DEBUG=1 to see debugging output (printed to STDERR)
DEBUG=0
## Assume files have headers by default.
HEADERS=1
## Default temp location
TMPDIR=/tmp


echo_debug () {
  if [ $DEBUG -eq 1 ]; then
    echo "$1" 1>&2
  fi
}

Usage () {
  echo "
Usage: $0 <pad_string> [opts] <file1> <file2> ... <fileN>

  Where 'opts' can be:

  --noheader[s]    Input files do not have column headers (default = TRUE)
  --tmpdir <dir>  Alternate temp file location. Default is '/tmp'

  NOTE:
    - Inputs MUST be TAB separated
    - Join column is ALWAYS the first column of each file

"
}

### START ###

## Process args and set options.
## Non args/options assumed to be file names.
shift
file_list=""
while [ ! -z "$1" ]; do
  case $1 in
    --noheader*|-nh) 
      HEADERS=0
      shift
      continue
      ;;
    --tmpdir)
      shift
      TMPDIR="$1"
      shift
      continue
      ;;
    --*|-*)
      ## Unknown flag - ignore
      shift
      continue
  esac
  ## Only add file if it exists
  if [ -f "$1" ]; then
    file_list="$file_list
$1"
  else
    echo "Error: Input file '$1' not found! Skipping.." 1>&2
  fi
  shift
done

num_files=`echo "$file_list" | grep "\w" | wc -l`
echo_debug "num_files=$num_files"


if [ $num_files -lt 2 ]; then
  echo "Error: Must supply at least 2 valid file names!
" 1>&2
  Usage
  exit 1
fi


## Build superset of IDs for join operation and remove headers (before adding
## back in later)
count=0
for f in $file_list
do
  if [ $HEADERS -eq 1 ]; then
    head -1 "$f" > $TMPDIR/$outfile.$count.header
    nlines=`wc -l "$f" | cut -d' ' -f 1`
    tail -`expr $nlines - 1` "$f" | cut -f 1 >> $TMPDIR/$outfile.ids
  else
    cut -f 1 "$f" >> $TMPDIR/$outfile.ids
  fi
  count=`expr $count + 1`
done

sort $TMPDIR/$outfile.ids | uniq > $TMPDIR/$outfile.ids.uniq
uniq_ids=`cat $TMPDIR/$outfile.ids.uniq | wc -l`
echo_debug "Merging on $uniq_ids unique IDs"

## Make column of unique IDs the base file for the join operation
cp $TMPDIR/$outfile.ids.uniq $TMPDIR/$outfile.join

## Iteratively merge file names to base file
for f in $file_list
do
  fbase=`basename $f`
  echo_debug "merging '$f'.."
  if [ $HEADERS -eq 1 ]; then
    nlines=`wc -l "$f" | cut -d' ' -f 1`
    tail -`expr $nlines - 1` "$f" | sort -k1,1 > $TMPDIR/$fbase.sorted
  else
    sort -k1,1 "$f" > $TMPDIR/$fbase.sorted
  fi
  join -j1 -a1 -t$'\t' $TMPDIR/$outfile.join $TMPDIR/$fbase.sorted > $TMPDIR/$outfile.join.tmp

  ## Determine number of columns in file "$f" so we know how many columns to pad with
  ## for IDs present in first file but not the second
  maxnf=`awk -F'\t' '{print NF}' $f | sort -n | tail -1`
  startnf=`awk -F'\t' '{print NF}' $TMPDIR/$outfile.join | head -1`
  targetnf=`expr $maxnf + $startnf - 1`
  echo_debug "start cols = $startnf"
  echo_debug "target cols = $targetnf"

  ## The padding happens here
  awk -F'\t' -v targetnf=$targetnf -v startnf=$startnf -v pad="$pad_string" 'BEGIN{ORS=""} {
    if (NF == startnf) {
      print $0;
      for (i=startnf; i<targetnf; i++) {
        print "\t"pad;
      }
      print "\n";
    } else {
      print $0"\n";
    }
  }' $TMPDIR/$outfile.join.tmp > $TMPDIR/$outfile.join.result
  mv $TMPDIR/$outfile.join.result $TMPDIR/$outfile.join

  echo_debug "=== head intermediate result: ==="
  ires=`head $TMPDIR/$outfile.join`
  echo_debug "$ires"
  echo_debug "=================================";
  echo_debug

done

if [ $HEADERS -eq 1 ]; then
  ## Merge headers and add to final output
  max=`expr $count - 1`
  count=0
  finalheader=`cat $TMPDIR/$outfile.$count.header`
  while [ $count -lt $max ]; do
    count=`expr $count + 1`
    ## trim first (join) col name from header (already there from file #1)
    nextheader=`cat $TMPDIR/$outfile.$count.header | sed -E "s|^[^\t]+\t||"`
    finalheader="${finalheader}\t${nextheader}"
  done
fi

## clean tmp files
rm -f $TMPDIR/$outfile.join.tmp $TMPDIR/$outfile.ids* $TMPDIR/*.sorted

## print result to STDOUT (user must redirect to a file or pipe into something else)
if [ $HEADERS -eq 1 ]; then
  /bin/echo -e "$finalheader"
fi
cat $TMPDIR/$outfile.join
