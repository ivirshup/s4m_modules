#!/usr/bin/perl -w
##===========================================================================
## Script: unpack_expression_table.pl
## Author: O.Korn
##
## "Unwind" an expression table into long format.
##===========================================================================

if (scalar(@ARGV) < 1) {
  print STDERR "Error: Must supply input file argument!\n";
  exit 1;
}

my $input = $ARGV[0];

## Replace spaces in header names with underscores, remove quotes
my $samples_header=`head -1 "$input" | sed -r -e 's/\ /\_/g' -e 's/\"//g'`;
my @samples = split(/\s/, $samples_header);
## Sometimes the first sample header column is null. If so, remove it.
shift @samples unless length($samples[0]);

my $nsamples = scalar(@samples);

die "Error: Could not open file '$input'!" unless open(INPUTFILE, "<", $input);
my @rows = <INPUTFILE>;
# Remove header row
shift(@rows);
close(INPUTFILE);

foreach (@rows) {
   chomp;
   my @tokens = split(/\s/, $_);

   ## skip if feature ID doesn't include more than one of: [A-Za-z0-9_]
   next unless $tokens[0] =~ /\w+/;
   ## skip if number of values !== number of samples (1:1 ratio of values [even if empty] to samples)
   if (scalar(@tokens) - 1 ne $nsamples) {
      print STDERR "Error: Skipped feature $tokens[0] - has " . (scalar(@tokens) - 1) . " values (expected $nsamples)\n";
      next;
   }

   my $feat_id = $tokens[0];
   $feat_id =~ s/\"//g;
   for (my $i = 1; $i < scalar(@tokens); $i++) {
      $sample_id = $samples[$i-1];
      $sample_id =~ s/\"//g;
      print $sample_id . "\t" . $feat_id . "\t" . $tokens[$i] . "\n";
   }
}
