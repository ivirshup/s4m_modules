#!/bin/sh
## test_ComBat.sh: For "unittest" module execution

## Set up test environment before any tests are run
oneTimeSetUp () {

  ## Load functions being tested
  . ./inc/ComBat_funcs.sh

  ## Set up temp data / files location and populate dummy files
  TMPDIR="$S4M_TMP/$$"
  mkdir "$TMPDIR"

  #echo "TMPDIR=[$TMPDIR]"   
}

## Run after all tests finished
oneTimeTearDown () {
  rm -rf "$TMPDIR"
}

## Run between tests
setUp () {
  ## skip
  echo
}


## TESTS ##

## Check that required binary dependencies exist.
## We're not reinventing the wheel here, so simply call the same core s4m
## function for dependency validation that would be run whenever the target
## module is invoked.
testBinaryDependencies () {
  s4m_validate_module_binary_dependencies "$UNITTEST_TARGET_MODULE"
  ret=$?
  assertEquals "   DEBUG: validate result = [$ret], UNNITTEST_TARGET_MODULE=[$UNITTEST_TARGET_MODULE]" 0 $ret
}

## Check that we have required libraries installed etc
## NOTE: This should move to 'Rutil' hooks in module.ini in future
##  but we don't have time for that now
testComBatREnvironment () {
  s4m_import "Rutil/Rutil.sh"

  ## Get user R override if exists
  ##   unittest::unittest_get_arg()
  R_BIN=`unittest_get_arg "R"`
  if [ -z "$R_BIN" ]; then
    R_BIN=R
  fi
  echo "   DEBUG: Using R=[$R_BIN]" 1>&2

  required_packages="sva>=3.4.0
"
  ## Rutil::check_R_libraries()
  check_R_libraries "$R_BIN" "$required_packages"
  ret=$?

  assertEquals 0 $ret
}

## Check that we have required libraries installed etc
## NOTE: This should move to 'Rutil' hooks in module.ini in future
##  but we don't have time for that now
testLimmaREnvironment () {
  s4m_import "Rutil/Rutil.sh"

  ## Get user R override if exists
  ##   unittest::unittest_get_arg()
  R_BIN=`unittest_get_arg "R"`
  if [ -z "$R_BIN" ]; then
    R_BIN=R
  fi
  echo "   DEBUG: Using R=[$R_BIN]" 1>&2

  required_packages="limma>=3.26.8
"
  ## Rutil::check_R_libraries()
  check_R_libraries "$R_BIN" "$required_packages"
  ret=$?

  assertEquals 0 $ret
}

