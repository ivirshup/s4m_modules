#!/bin/sh
## test_samtools: For "unittest" module execution
## To be executed inside module directory

## Set up test environment before any tests are run
oneTimeSetUp () {
  #SUBREAD_TEST_FQDIR="./data"
  #export SUBREAD_TEST_FQDIR
 
  ## Load functions being tested
  #. ./inc/subread_funcs.sh

  ## Set up temp data / files location and populate dummy files
  mkdir "$S4M_TMP/$$"
   
}

## Run after all tests finished
oneTimeTearDown () {
  rm -rf "$S4M_TMP/$$"
}

## Run between tests
setUp () {
  #rm -rf "$S4M_TMP/$$/"*.tab
  #touch "$S4M_TMP/$$/foo.00.c.tab"
  :
}



## TESTS ##


testSamtoolsExists () {
  which samtools > /dev/null 2>&1
  assertEquals $? 0 
}


testSamtoolsVersion_0_1_19 () {
  samtools 2>&1 | grep "Version\:" | grep "0.1.19" > /dev/null 2>&1
  assertEquals $? 0 
}

